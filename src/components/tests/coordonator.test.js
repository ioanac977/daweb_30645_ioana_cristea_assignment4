import React from 'react';
import renderer from 'react-test-renderer';
import ReactDOM from 'react-dom';
import Coordonator from "../coordonator";
import { act } from 'react-dom/test-utils';

let container;

beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
});

afterEach(() => {
    document.body.removeChild(container);
    container = null;
});

it('Coordonator Label and Button matching values', () => {
    // Test first render and componentDidMount
    act(() => {
        ReactDOM.render(<Coordonator />, container);
    });
    const button = container.querySelector('button');
    const label = container.querySelector('p');
    expect(label.textContent).toBe('Computer Networks and Internet Technologies | Distributed Systems | High Performance Computing | Advanced Architectures for Parallel Computer ');
    expect(document.title).toBe("");


});
