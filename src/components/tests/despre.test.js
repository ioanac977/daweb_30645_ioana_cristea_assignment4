import React from 'react';
import Despre from '../Despre';
import renderer from 'react-test-renderer';


test('Despre field modify',()=>{
    const component = renderer.create(<Despre titlu="Titlu schimbat"/>);
    const instance = component.getInstance()
    expect(instance.state.titlu).toBe("")
    instance.changeTitle();
    expect(instance.state.titlu).toBe("Titlu schimbat")
});

