import Redirect from "react-router-dom/Redirect";
import React, {Component} from "react";

export default class Logout extends Component {

    constructor(props){
        super(props);
        this.state = {
            email:'',
            password: '',
            userDetails:{},
            redirectToReferrer: false
        };

        this.logout = this.logout.bind(this);
        this.onChange = this.onChange.bind(this);

    }
    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }
    logout(){
        sessionStorage.setItem("userData",'');
        sessionStorage.clear();
        this.setState({redirectToReferrer: true});
    }
    componentDidMount() {
        this.logout();
    }



render() {
    if (this.state.redirectToReferrer) {
        return (<Redirect to={'/login'}/>)
    }
        return (<div>


        </div>);
    }
}
