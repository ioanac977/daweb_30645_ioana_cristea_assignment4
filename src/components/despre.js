import React, {Component} from 'react';
import {CardText, Cell} from 'react-mdl';
import "../styles/despre.css"
import getTranslation from "../constants/getTranslation";
import {store} from "../constants/store"
import {TextareaAutosize} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import ApiServiceEmail from "../service/ApiServiceEmail";
import ApiServiceComments from "../service/ApiServiceComments";
import Redirect from "react-router-dom/Redirect";
import ApiServiceUser from "../service/ApiServiceUser";
import {Chart} from "react-google-charts";
import Idle from "react-idle";

// eslint-disable-next-line
class Despre extends Component {

    constructor(props){
        super(props);
        this.state ={
            description:'',
            user_id:1,
            redirectToReferrer:false,
            titlu:'',
            message: null
        }
         this.addComment = this.addComment.bind(this);
        this.loadUser = this.loadUser.bind(this);
    }

    componentDidMount() {
        this.loadUser();

    }

    changeTitle(){
        this.setState(() => {
            return { titlu: "Titlu schimbat" };
        });
    }

    loadUser() {
        if(window.localStorage.getItem("emailUtilizatorLogat")) {
            ApiServiceUser.getUserIdByEmail(window.localStorage.getItem("emailUtilizatorLogat"))
                .then((res) => {
                    let user = res.data[0];
                    this.setState({
                        user_id: user.id,

                    })
                });
        }
    }
    componentWillMount = () => {
        this.selectedCheckboxes = new Set();
        if (sessionStorage.getItem("userData")) {
            // this.getUserFeed();
        } else {
            this.setState({redirectToReferrer: true});
        }
    }
    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    addComment = (e) => {
        e.preventDefault();

        if (this.state.redirectToReferrer) {
           this.props.history.push('/login');
        }else {
            let comment = {description: this.state.description, user_id: this.state.user_id};

            ApiServiceComments.addComment(comment)
                .then(res => {
                    this.setState({message: 'Comment added successfully.'});
                    this.props.history.push('/success');
                });
        }

    }
    viewChart()
    {



                    this.props.history.push('/charts');


    }

    render() {
        return (

            <div>
                <div className="landing-grid">


                </div>

                <div className="text-box">

                    <h1 className="heading-primary">
                        <span className="heading-primary-main">Planning Gps</span>
                        <span className="heading-primary-sub">using</span>
                        <span className="heading-primary-main">Google APIs</span>
                    </h1>


                </div>

                <div className="container">

                    <div className="sticky-notes">
                        <form action="get" id="survey-form">

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 1:</legend>
                                <fieldset>
                                    <legend>User authentication/creation:</legend>
                                    <CardText>Sign in</CardText>
                                    <CardText>Register</CardText>
                                    <CardText>CRUD Account</CardText>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 2:</legend>
                                <fieldset>
                                    <legend>Choose the interest area:</legend>
                                    <br>
                                    </br>
                                    <select name="location" id="dropdown">
                                        <optgroup label="Africa">
                                            <option value="">Algeria</option>
                                            <option value="">Botswana</option>
                                            <option value="">Madagascar</option>
                                            <option value="">Uganda</option>
                                        </optgroup>
                                        <optgroup label="Asia">
                                            <option value="">Afghanistan</option>
                                            <option value="">India</option>
                                            <option value="">Pakistan</option>
                                            <option value="">Russia</option>
                                        </optgroup>
                                        <optgroup label="Europe">
                                            <option value="">Albania</option>
                                            <option value="">Croatia</option>
                                            <option value="">Georgia</option>
                                            <option value="">Romania</option>
                                        </optgroup>
                                        <optgroup label="South America">
                                            <option value="">Argentina</option>
                                            <option value="">Brazil</option>
                                            <option value="">Chile</option>
                                            <option value="">Venezuela</option>
                                        </optgroup>
                                    </select>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 3:</legend>
                                <fieldset>
                                    <legend> Create a plan:</legend>
                                    <CardText>Select a week</CardText>
                                    <CardText> Add pins on locations</CardText>
                                    <CardText> CRUD cell activity</CardText>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 4:</legend>
                                <fieldset>
                                    <legend> Plan management:</legend>
                                    <CardText>View plans</CardText>
                                    <CardText> View activity charts</CardText>
                                    <CardText> Smart routes</CardText>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 5:</legend>
                                <fieldset>
                                    <legend>Interface and app marketing:</legend>
                                    <CardText>Choose interface theme</CardText>
                                    <CardText> Rating the application </CardText>
                                    <CardText> View latest rates</CardText>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 6:</legend>
                                <fieldset>
                                    <legend>Security:</legend>
                                    <CardText>Encrypted password</CardText>
                                    <CardText> Routes security </CardText>
                                    <CardText> Cookie session</CardText>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 7:</legend>
                                <fieldset>
                                    <legend>Usability:</legend>
                                    <CardText>Friendly interface</CardText>
                                    <CardText> Guest session </CardText>
                                    <CardText> Demo </CardText>
                                </fieldset>
                            </fieldset>

                        </form>
                        <label>
                            Comment :
                        </label>
                        <br></br>
                        <input style={{marginLeft:"-10px",width:"550px",height:"120px",background:"rgb(217, 239, 203)"}} placeholder="description" fullWidth margin="normal" name="description" value={this.state.description} onChange={this.onChange}/>
                        <br></br>
                        <Button style={{marginLeft:"-10px",width:"550px",height:"30px",background:"white"}} variant="contained" onClick={this.addComment}>Add Comment</Button>
                        <br></br>





                    </div>
                    <br></br>
                    <div className={"my-pretty-chart-container"}>
                        <Chart
                            width={1000}
                            height={350}
                            chartType="Calendar"
                            loader={<div>Loading Chart</div>}

                            data={[
                                [{ type: 'date', id: 'Date' }, { type: 'number', id: 'Won/Loss' }],
                                [new Date(2019, 3, 13,8,3,6), 37],
                                [new Date(2019, 3, 14), 38],
                                [new Date(2019, 3, 15), 24],
                                [new Date(2019, 3, 16), 108],
                                [new Date(2019, 3, 17), 39],
                                [new Date(2020, 1, 4), 21],
                                [new Date(2020, 1, 5), 70],
                                [new Date(2020, 1, 12), 190],
                                [new Date(2020, 1, 13), 29],
                                [new Date(2020, 1, 19), 33],
                                [new Date(2020, 1, 23), 45],
                                [new Date(2020, 1, 24), 36],
                                [new Date(2020, 2, 10), 127],
                            ]}
                            options={{
                                title: 'Comments by day',
                            }}
                            rootProps={{ 'data-testid': '1' }}
                        />
                        <div>
                            <Idle
                                timeout={10000}
                                render={({ idle }) =>
                                    <h1>
                                        {idle
                                            ?<Redirect to={'/logout'}/>
                                            : ""
                                        }
                                    </h1>

                                }
                            />
                        </div>
                    </div>
                    <footer>
                        Made by <a href ="https://github.com/ioanac977/presentation-resume-template">@IoanaCristea</a>
                    </footer>

                </div>

            </div>


        )
    }
}

export default Despre;
