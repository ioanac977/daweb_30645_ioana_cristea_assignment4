import axios from 'axios';

const COMMENTS_API_BASE_URL = 'http://localhost:8000/api/comments';

class ApiServiceEmail {


    addComment(comment) {
        return axios.post(""+COMMENTS_API_BASE_URL, comment);
    }

}

export default new ApiServiceEmail();
