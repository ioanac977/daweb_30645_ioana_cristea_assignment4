import axios from 'axios';

const USER_API_BASE_URL = 'http://localhost:8000/api/UserId';

class ApiServiceUser {

    getUserIdByEmail(email) {
        return axios.get(USER_API_BASE_URL+'/'+email);
    }

}

export default new ApiServiceUser();
