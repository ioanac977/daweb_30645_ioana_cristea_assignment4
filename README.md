# DESCRIPTION 
Starting from the web application developed in assignment 3, implement the following new functionalities.
- Roles security: 
```
		 Unauthorized user : Cannot add comments & Cannot edit profile 
		 Authorized user : Can add comments & Can edit own profile 
```
# REQUIREMENTS
- Implement and integrate in the application the following new services
```
	 Report / chart to provide statistics for the number of comments received per day (Google Chart Service)
	 Show on the map the suggested path from the user's computer to the contact address in application. (Google Map Service)
```
- Define test cases for new features. Use tools and testing techniques to them validate (Unit Tests)
- Implement the necessary functionalities to comply with security principles for web applications (Sessions, AWT)
# REMARKS 
- New features will be implemented using any web programming language (React JS)
# CONFIGURATIONS 
- install NodeJs 
- install WebStorm
# RUN
- download repository
## FRONTEND
- open frontend project using WebStorm
- go to src directory 
- run on terminal "npm install"
- run on terminal "npm start"
# NAVIGATION 
- the frontend application will start on http://localhost:3000/
 